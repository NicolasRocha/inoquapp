package com.proyect.inoquapp.controllers;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.entities.ProductionOrder;
import com.proyect.inoquapp.services.ProductionOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/production")
public class ProductionOrderController {

    private ProductionOrderService productionOrderService;

    @Autowired
    public ProductionOrderController(ProductionOrderService productionOrderService){
        this.productionOrderService = productionOrderService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<ProductionOrder> findAll(){
        return productionOrderService.findAll();
    }

    @RequestMapping(value = ("/find"), method = RequestMethod.GET)
    public ProductionOrder findById(@RequestParam("id") Long id){
        return productionOrderService.findById(id);
    }
}
