package com.proyect.inoquapp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReceptionOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String lot;

    @ManyToOne
    private RawMaterial rawMaterial;

    @ManyToOne
    private User receiver;

    @ManyToOne
    private Supplier supplier;

    @DateTimeFormat
    private Date receptionDate;

    private List<String> transportResults;
    private List<String> analysisResults;

    @DateTimeFormat
    private Date approvalDate;
    @DateTimeFormat
    private Date expirationDate;

    private Boolean aproved;
}
