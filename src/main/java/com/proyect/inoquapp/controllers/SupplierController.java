package com.proyect.inoquapp.controllers;

import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.entities.Supplier;
import com.proyect.inoquapp.services.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    private SupplierService supplierService;

    @Autowired
    public SupplierController(SupplierService supplierService){
        this.supplierService = supplierService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Supplier> findAll(){
        return supplierService.findAll();
    }

    @RequestMapping(value = ("/find"), method = RequestMethod.GET)
    public Supplier findByRne(@RequestParam("rne") String rne){
        return supplierService.findByRne(rne);
    }
}
