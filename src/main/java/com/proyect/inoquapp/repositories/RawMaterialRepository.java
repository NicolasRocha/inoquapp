package com.proyect.inoquapp.repositories;

import com.proyect.inoquapp.entities.RawMaterial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RawMaterialRepository extends JpaRepository<RawMaterial, Long> {

}
