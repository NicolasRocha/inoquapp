package com.proyect.inoquapp.controllers;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.services.RawMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/raw")
public class RawMaterialController {

    private RawMaterialService rawMaterialService;

    @Autowired
    public RawMaterialController(RawMaterialService rawMaterialService){
        this.rawMaterialService = rawMaterialService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<RawMaterial> findAll(){
        return rawMaterialService.findAll();
    }

    @RequestMapping(value = ("/find"), method = RequestMethod.GET)
    public RawMaterial findById(@RequestParam("id") Long id){
        return rawMaterialService.findById(id);
    }
}
