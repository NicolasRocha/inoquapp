package com.proyect.inoquapp.repositories;

import com.proyect.inoquapp.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    public User findByLastName(String lastName);
}
