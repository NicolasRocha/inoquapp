package com.proyect.inoquapp.repositories;

import com.proyect.inoquapp.entities.FinishedProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FinishedProductsRepository extends JpaRepository<FinishedProduct, String> {

    public FinishedProduct findByRnpa(String rnpa);

}
