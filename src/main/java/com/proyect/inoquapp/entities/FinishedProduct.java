package com.proyect.inoquapp.entities;

import com.proyect.inoquapp.enums.Analysis;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class FinishedProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String rnpa;

    private String name;

    @ManyToMany
    private List<RawMaterial> rawMaterials;

    private List<Float> quantity;

    private List<Analysis> analysis;

}
