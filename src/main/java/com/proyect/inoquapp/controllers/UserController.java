package com.proyect.inoquapp.controllers;

import com.proyect.inoquapp.entities.User;
import com.proyect.inoquapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<User> findAll(){
        return userService.findAll();
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public User findByLastName(@RequestParam("lastName") String lastName){
        return userService.findByLastName(lastName);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createUser(@RequestBody User user){
        if (userService.createUser(user)){
            return "Usuario creado correctamente";
        } else {
            return "Alguno de los datos no fue ingresado correctamente.";
        }
    }
}
