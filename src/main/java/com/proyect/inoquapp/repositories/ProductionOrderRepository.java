package com.proyect.inoquapp.repositories;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.entities.ProductionOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductionOrderRepository extends JpaRepository<ProductionOrder, Long> {
}
