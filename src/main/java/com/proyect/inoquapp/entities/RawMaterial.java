package com.proyect.inoquapp.entities;

import com.proyect.inoquapp.enums.Analysis;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RawMaterial {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany
    private List<Supplier> supplier;

    @OneToMany
    private List<ReceptionOrder> receptionOrders;

    @ManyToMany
    private List<FinishedProduct> finishedProducts;

    private List<Analysis> analysis;

    private List<String> transportConditions;
    private List<String> analysisResults;
}
