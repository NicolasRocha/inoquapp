package com.proyect.inoquapp.repositories;

import com.proyect.inoquapp.entities.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {

    public Supplier findByRne(String rne);
}
