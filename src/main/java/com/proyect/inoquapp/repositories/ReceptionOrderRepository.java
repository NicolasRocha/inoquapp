package com.proyect.inoquapp.repositories;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.entities.ReceptionOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceptionOrderRepository extends JpaRepository<ReceptionOrder, Long> {
}
