package com.proyect.inoquapp.entities;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class ProductionOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private FinishedProduct finishedProduct;

    @ManyToOne
    private User productionManager;

    @ManyToMany
    private List<User> productionStaff;

    @ManyToMany
    private List<RawMaterial> rawMaterialUsed;

    private List<Float> quantityOfRawMaterials;
}
