package com.proyect.inoquapp.services;

import com.proyect.inoquapp.entities.User;
import com.proyect.inoquapp.enums.Position;
import com.proyect.inoquapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    private boolean verifyUser(User user){
        return verifyAge(user.getAge()) &&
                verifyDNI(user.getDni()) &&
                verifyName(user.getName()) &&
                verifyName(user.getLastName()) &&
                verifyPosition(user.getPosition()) &&
                verifyMail(user.getMail());
    }
    public boolean verifyName(String name){
        return (name.length() < 20);
    }

    public boolean verifyDNI(String dni){
        return (dni.length() == 8);
    }

    public boolean verifyMail(String mail){
        return (mail.contains("@"));
    }

    public boolean verifyPosition(String position){
        for (Position pos: Position.values()) {
            if (pos.equals(position.toUpperCase())){
                return true;
            }
        }
        return false;
    }

    public boolean verifyAge(Integer age){
        return ((age > 18) && (age > 65));
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User findByLastName(String lastName){
        return userRepository.findByLastName(lastName);
    }

    public Boolean createUser(User user){
        if (verifyUser(user)){
            userRepository.save(user);
            return true;
        } else {
            return false;
        }
    }
}
