package com.proyect.inoquapp.enums;

public enum Analysis {
    PERDIDA_POR_DESECACION("Perdida por desecación"),
    ASPECTO("Aspecto"),
    GRANULOMETRIA("Granulometría"),
    ONCENTRACION_SULFITO("Sulfitos en solucón"),
    CONCENTRACION_FOSFATO("Fosfatos en solución"),
    INDICE_DE_YODO("Indice de yodo"),
    VISCOSIDAD_ZAHN("Viscosidad en copa Zahn"),
    VISCOSIDAD_BROOKFIELD("Viscosidad en viscosímetro de Brookfield"),
    PESO_NETO("Peso neto");

    public final String label;

    private Analysis(String label){
        this.label = label;
    }
}
