package com.proyect.inoquapp.services;

import com.proyect.inoquapp.entities.Supplier;
import com.proyect.inoquapp.repositories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierService {

    private SupplierRepository supplierRepository;

    @Autowired
    public SupplierService(SupplierRepository supplierRepository){
        this.supplierRepository = supplierRepository;
    }

    public List<Supplier> findAll(){
        return supplierRepository.findAll();
    }

    public Supplier findByRne(String rne){
        return supplierRepository.findByRne(rne);
    }

    public Boolean verifyRne(String rne){
        return (!(rne.isEmpty() || rne.equals("")));
    }

    public Boolean verifyName(String name){
        return (!(name.isBlank()));
    }

    public Boolean verifyContactName(String name){
        return (!(name.isBlank()));
    }

    public Boolean verifyContactMail(String mail){
        return (mail.contains("@") && !mail.isBlank());
    }

    public Boolean verifyPhoneNumber(String phone){
        return (phone.length() == 10);
    }

    public Boolean qualify(int newScore, Supplier supplier){
        if (0 < newScore && newScore <= 5){
            Float qualification;
            if (supplier.getQualification() == null){
                qualification = (float) newScore;
            } else {
                qualification = (supplier.getQualification() + newScore) / 2;
            }
            supplier.setQualification(qualification);
            supplierRepository.save(supplier);
            return true;
        } return false;
    }

}
