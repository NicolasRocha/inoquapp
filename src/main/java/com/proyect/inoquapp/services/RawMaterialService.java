package com.proyect.inoquapp.services;

import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.entities.Supplier;
import com.proyect.inoquapp.enums.Analysis;
import com.proyect.inoquapp.repositories.RawMaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RawMaterialService {

    private RawMaterialRepository rawMaterialRepository;
    private SupplierService supplierService;

    @Autowired
    public RawMaterialService(RawMaterialRepository rawMaterialRepository, SupplierService supplierService){
        this.rawMaterialRepository = rawMaterialRepository;
        this.supplierService = supplierService;
    }

    /**
     * This method returns a list of all the production orders from the database.
     * @return List
     */
    public List<RawMaterial> findAll(){
        return rawMaterialRepository.findAll();
    }

    /**
     * Gets a raw material from the database using its id.
     * @param id long
     * @return ProductionOrder
     */
    public RawMaterial findById(Long id){
        return rawMaterialRepository.findById(id).orElse(null);
    }

    /**
     * While trying to insert a new raw material order into the database, this method verifies that the
     * raw material that will be produced is in the database.
     * @param suppliers list of approved suppliers for an specific raw material
     * @return Boolean
     */
    private Boolean verifySupplier(List<Supplier> suppliers){

        List<Supplier> auxList = supplierService.findAll();

        for (Supplier supplier: suppliers) {
            if (! auxList.contains(supplier)){
                return false;
            }
        } return true;
    }

    /**
     * This method adds a supplier into the List of suppliers of a specific raw material. The supplier must be in
     * the suppliers database.
     * @param rawMaterial RawMaterial that will get the new supplier
     * @param supplier Supplier that is already into the database.
     * @return
     */
    public Boolean addSupplier(RawMaterial rawMaterial, Supplier supplier){
        if (!rawMaterial.getSupplier().contains(supplier)){
            rawMaterial.getSupplier().add(supplier);
            rawMaterialRepository.save(rawMaterial);
            return true;
        } else {
            return false;
        }
    }

}
