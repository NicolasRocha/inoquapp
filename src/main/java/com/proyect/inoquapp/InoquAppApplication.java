package com.proyect.inoquapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InoquAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(InoquAppApplication.class, args);
    }

}
