package com.proyect.inoquapp.enums;

public enum Position {
    RESPONSABLE_PRODUCCION, PERSONAL_PRODUCCION, RESPONSABLE_CALIDAD, PERSONAL_CALIDAD, PERSONAL_ADMINISTRATIVO,
    ADMINISTRADOR
}
