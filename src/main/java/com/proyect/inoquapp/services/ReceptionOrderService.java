package com.proyect.inoquapp.services;

import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.entities.ReceptionOrder;
import com.proyect.inoquapp.entities.Supplier;
import com.proyect.inoquapp.entities.User;
import com.proyect.inoquapp.enums.Position;
import com.proyect.inoquapp.repositories.ReceptionOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ReceptionOrderService {

    private ReceptionOrderRepository receptionOrderRepository;
    private RawMaterialService rawMaterialService;

    @Autowired
    public ReceptionOrderService(ReceptionOrderRepository receptionOrderRepository, RawMaterialService rawMaterialService){
        this.receptionOrderRepository = receptionOrderRepository;
        this.rawMaterialService = rawMaterialService;
    }

    public List<ReceptionOrder> findAll(){
        return receptionOrderRepository.findAll();
    }

    public ReceptionOrder findById(Long id){
        return receptionOrderRepository.findById(id).orElse(null);
    }

    private Boolean verifyRawMaterial(RawMaterial rawMaterial){

        List<RawMaterial> auxList = rawMaterialService.findAll();

        return auxList.contains(rawMaterial);
    }

    private Boolean verifyLot(ReceptionOrder receptionOrder){
        if (receptionOrder.getLot().isEmpty() || receptionOrder.getLot().equals("")){
            return false;
        } return true;
    }

    private Boolean verifyExpirationDate(ReceptionOrder receptionOrder){
        Date today = new Date();
        receptionOrder.setReceptionDate(today);
        return receptionOrder.getExpirationDate().after(today);
    }

    private Boolean verifyReceiver(User receiver){
        return (receiver.getPosition().equals(Position.PERSONAL_PRODUCCION) ||
                receiver.getPosition().equals(Position.RESPONSABLE_PRODUCCION));
    }

    private Boolean verifySupplier(ReceptionOrder receptionOrder){

        List<Supplier> auxList = receptionOrder.getRawMaterial().getSupplier();
        return auxList.contains(receptionOrder.getSupplier());
    }

    private Boolean verifyTransportResults(ReceptionOrder receptionOrder){

        List<String> auxList =receptionOrder.getRawMaterial().getTransportConditions();
        return auxList.equals(receptionOrder.getTransportResults());
    }

    private Boolean verifyAnalysisResult(ReceptionOrder receptionOrder){

        List<String> auxList = receptionOrder.getRawMaterial().getAnalysisResults();
        return receptionOrder.getAnalysisResults().equals(auxList);
    }

    private void approve(ReceptionOrder receptionOrder){

        if (verifyRawMaterial(receptionOrder.getRawMaterial()) &&
                verifyExpirationDate(receptionOrder) &&
                verifyLot(receptionOrder) &&
                verifySupplier(receptionOrder) &&
                verifyReceiver(receptionOrder.getReceiver()) &&
                verifyTransportResults(receptionOrder) &&
                verifyAnalysisResult(receptionOrder)
            ){

            receptionOrder.setAproved(true);
            receptionOrder.setApprovalDate(new Date());
            receptionOrderRepository.save(receptionOrder);
        }
    }
}
