package com.proyect.inoquapp.services;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.repositories.FinishedProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinishedProductService {

    final private FinishedProductsRepository finishedProductsRepository;
    final private RawMaterialService rawMaterialService;

    @Autowired
    public FinishedProductService(FinishedProductsRepository finishedProductsRepository,
                                  RawMaterialService rawMaterialService){
        this.finishedProductsRepository = finishedProductsRepository;
        this.rawMaterialService = rawMaterialService;
    }

    /**
     * This method returns a list of all the products from the database.
     * @return List<FinishedProduct>
     */
    public List<FinishedProduct> findAll(){
        return finishedProductsRepository.findAll();
    }

    /**
     * Takes the RNPA number, and returns the Finished Product identified by it.
     * @param rnpa String
     * @return FinishedProduct
     */
    public FinishedProduct findByRnpa(String rnpa){
        return finishedProductsRepository.findByRnpa(rnpa);
    }

    /**
     * While trying to insert a new finished product into the database, this method verifies that the
     * rnpa entered is correct.
     * @param rnpa String
     * @return Boolean
     */
    private Boolean verifyRnpa(String rnpa){
        return (rnpa.length() <= 10);
    }

    /**
     * While trying to insert a new finished product into the database, this method verifies that the
     * raw materials it uses is in the RawMaterial table in the database.
     * @param rawMaterials RawMaterial
     * @return Boolean
     */
    private Boolean verifyRawMaterials(List<RawMaterial> rawMaterials){

        List<RawMaterial> auxList = rawMaterialService.findAll();

        for (RawMaterial rawMaterial: rawMaterials) {
            if (!auxList.contains(rawMaterial)){
                return false;
            }
        } return true;
    }

    /**
     * While trying to insert a new finished product into the database, this method verifies that the
     * data entered by the user satisfies the requirements.
     * @param finishedProduct FinishedProduct
     * @return boolean
     */
    private Boolean verifyProduct(FinishedProduct finishedProduct){
        return (verifyRnpa(finishedProduct.getRnpa()) &&
                verifyRawMaterials(finishedProduct.getRawMaterials()));
    }

    /**
     * While trying to insert a new finished product into the database, this method verifies that the
     * data entered by the user is correct and if it is, inserts the product into the database.
     * @param finishedProduct FinishedProduct
     * @return boolean
     */
    private Boolean createProduct(FinishedProduct finishedProduct){
        if (verifyProduct(finishedProduct)){
            finishedProductsRepository.save(finishedProduct);
            return true;
        } else {
            return false;
        }
    }
}
