package com.proyect.inoquapp.entities;

import com.proyect.inoquapp.enums.Position;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastName;
    private String dni;
    private String mail;
    private String password;
    private Position position;
    private Integer age;

    @DateTimeFormat
    private Date startingDate;
    @DateTimeFormat
    private Date dischargeDate;
}
