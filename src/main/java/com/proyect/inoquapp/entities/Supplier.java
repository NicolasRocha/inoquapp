package com.proyect.inoquapp.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Supplier {

    @Id
    private String rne;
    private String name;
    private String contactName;
    private String contactMail;
    private String phoneNumber;

    @ManyToMany
    private List<RawMaterial> rawMaterial;

    @OneToMany
    private List<ReceptionOrder> receptionOrderList;

    private Float qualification;
}
