package com.proyect.inoquapp.controllers;

import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.entities.ReceptionOrder;
import com.proyect.inoquapp.services.ReceptionOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/reception")
public class ReceptionOrderController {

    private ReceptionOrderService receptionOrderService;

    @Autowired
    public ReceptionOrderController(ReceptionOrderService receptionOrderService){
        this.receptionOrderService = receptionOrderService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<ReceptionOrder> findAll(){
        return receptionOrderService.findAll();
    }

    @RequestMapping(value = ("/find"), method = RequestMethod.GET)
    public ReceptionOrder findById(@RequestParam("id") Long id){
        return receptionOrderService.findById(id);
    }
}
