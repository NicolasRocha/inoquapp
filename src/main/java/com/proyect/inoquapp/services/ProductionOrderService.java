package com.proyect.inoquapp.services;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.entities.ProductionOrder;
import com.proyect.inoquapp.entities.RawMaterial;
import com.proyect.inoquapp.entities.User;
import com.proyect.inoquapp.enums.Position;
import com.proyect.inoquapp.repositories.ProductionOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductionOrderService {

    private ProductionOrderRepository productionOrderRepository;
    private FinishedProductService finishedProductService;

    @Autowired
    public ProductionOrderService(ProductionOrderRepository productionOrderRepository, FinishedProductService finishedProductService){
        this.productionOrderRepository = productionOrderRepository;
        this.finishedProductService = finishedProductService;
    }

    /**
     * This method returns a list of all the production orders from the database.
     * @return List
     */
    public List<ProductionOrder> findAll(){
        return productionOrderRepository.findAll();
    }

    /**
     * Gets a production order from the database using its id.
     * @param id long
     * @return ProductionOrder
     */
    public ProductionOrder findById(Long id){
        return productionOrderRepository.findById(id).orElse(null);
    }

    /**
     * While trying to insert a new production order into the database, this method verifies that the
     * finished product that will be produced is in the database
     * @param finishedProduct Finished product that will be elaborated
     * @return true if the finished product is in the database
     */
    private Boolean verifyFinishedProduct(FinishedProduct finishedProduct){
        List<FinishedProduct> auxList = finishedProductService.findAll();
        return auxList.contains(finishedProduct);
    }

    /**
     * While trying to insert a new production order into the database, this method verifies that the
     * production manager that will lead the production is registered
     * @param productionManager employee that will lead the production
     * @return true if the production manager is registered
     */
    private Boolean verifyProductionManager(User productionManager){
        return (productionManager.getPosition().equals(Position.RESPONSABLE_PRODUCCION));
    }

    /**
     * While trying to insert a new production order into the database, this method verifies that the
     * production staff that will participate in the production registered as production staff or production
     * laders.
     * @param productionStaff employees that will particiopate in the production but will not lead it
     * @return boolean
     */
    private Boolean verifyProductionStaff(List<User> productionStaff){
        for (User staff: productionStaff) {
            if (! (staff.getPosition().equals(Position.PERSONAL_PRODUCCION) ||
                    staff.getPosition().equals(Position.RESPONSABLE_PRODUCCION))){
                return false;
            }
        } return true;
    }

    /**
     * While trying to insert a new production order into the database, this method verifies that the
     * list of raw materials used in the production is equal to the list of raw materials required for that
     * specific finished product.
     * @param productionOrder the production order to be verified.
     * @return boolean
     */
    private Boolean verifyRawMaterialUsed(ProductionOrder productionOrder){

        List<RawMaterial> auxList = productionOrder.getFinishedProduct().getRawMaterials();

        for (RawMaterial used: productionOrder.getRawMaterialUsed()) {
            if (!auxList.contains(used)){
                return false;
            }
        } return true;
    }

    /**
     * While trying to insert a new production order into the database, this method verifies that the
     * quantity of raw materials used in the production is equal to the quantity of raw materials required for that
     * specific finished product.
     * @param productionOrder the production order to be verified.
     * @return boolean
     */
    private Boolean verifyQuantity(ProductionOrder productionOrder){

        List<Float> auxList = productionOrder.getFinishedProduct().getQuantity();

        for (int i = 0; i < (auxList.size() - 1); i++) {
            if (auxList.get(i) != productionOrder.getQuantityOfRawMaterials().get(i)){
                return false;
            }
        } return true;
    }

    /**
     * While trying to insert a new production order into the database, this method verifies that the
     * data entered by the user satisfies the requirements.
     * @param productionOrder
     * @return
     */
    private Boolean verifyProductionOrder(ProductionOrder productionOrder){

        if (verifyFinishedProduct(productionOrder.getFinishedProduct()) &&
        verifyProductionStaff(productionOrder.getProductionStaff()) &&
        verifyProductionManager(productionOrder.getProductionManager()) &&
        verifyRawMaterialUsed(productionOrder) &&
        verifyQuantity(productionOrder)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * While trying to insert a new finished productionOrder into the database, this method verifies that the
     * data entered by the user is correct and if it is, inserts the productionOrder into the database.
     * @param productionOrder FinishedProduct
     * @return boolean
     */
    public Boolean createProductionOrder(ProductionOrder productionOrder){
        if (verifyProductionOrder(productionOrder)){
            productionOrderRepository.save(productionOrder);
            return true;
        } return false;
    }
}
