package com.proyect.inoquapp.controllers;

import com.proyect.inoquapp.entities.FinishedProduct;
import com.proyect.inoquapp.services.FinishedProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class FinishedProductController{

    private FinishedProductService finishedProductService;

    @Autowired
    public FinishedProductController(FinishedProductService finishedProductService){
        this.finishedProductService = finishedProductService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<FinishedProduct> findAll(){
        return finishedProductService.findAll();
    }

    @RequestMapping(value = ("/find"), method = RequestMethod.GET)
    public FinishedProduct findByRnpa(@RequestParam("rnpa") String rnpa){
        return finishedProductService.findByRnpa(rnpa);
    }
}
